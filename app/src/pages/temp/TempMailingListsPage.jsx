import React, { useEffect } from "react";
import Title from "../../components/Title";
import Card from "../../components/Card";
import Path from "../../components/Path";
import Footer from "../../components/Footer";

const TempMailingListsPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const links = [
    {
      title: "Mailing list n°1",
      url: "",
      tags: ["Rewriting"],
    },
    {
      title: "Mailing list n°2",
      url: "",
      tags: ["Lambda Calculus"],
    },
    {
      title: "Mailing list n°3",
      url: "",
      tags: ["Logic"],
    },
    {
      title: "Mailing list n°4",
      url: "",
      tags: ["Rewriting", "Logic", "Lambda Calculus"],
    },
    {
      title: "Mailing list n°5",
      url: "",
      tags: ["Rewriting"],
    },
    {
      title: "Mailing list n°6",
      url: "",
      tags: ["Rewriting"],
    },
    {
      title: "Mailing list n°7",
      url: "",
      tags: ["Rewriting", "Logic", "Lambda Calculus"],
    },
    {
      title: "Mailing list n°8",
      url: "",
      tags: ["Logic"],
    },
  ];

  const paths = [
    {
      name: "Home",
      path: "/rewriting/temp",
    },
    {
      name: "Mailing Lists",
      path: "/rewriting/temp/mailing-lists",
    },
  ];

  return (
    <div className="container">
      <div className="main no-nav-bar">
        <Path paths={paths} />
        <Title title="Mailing lists" />
        <div className="industrial-applications-cards-container">
          {links.map(({ title, url, tags }) => (
            <Card title={title} url={url} tags={tags} />
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default TempMailingListsPage;
