import React from "react";
import LoginPageRef from "../pages/LoginPageRef";

const RequireAuth = (props) => {
  if (localStorage.getItem("authenticated")) {
    return <div style={{ width: "100%" }}>{props.children}</div>;
  } else {
    return <LoginPageRef />;
  }
};

export default RequireAuth;
