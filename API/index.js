const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const multer = require("multer");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const cookieParser = require("cookie-parser");
const crypto = require("crypto");

const upload = multer();
const app = express();

//Schemas
const User = require("./models/user");
const Researcher = require("./models/researcher");
const Company = require("./models/company");
const Text = require("./models/text");
const Conference = require("./models/conference");
const Workshop = require("./models/workshop");
const Tool = require("./models/tool");
const OpenProblem = require("./models/openproblem");
const Competition = require("./models/competition");
const Book = require("./models/book");
const Course = require("./models/course");
const Other = require("./models/other");

const salt = bcrypt.genSaltSync(10);
const secret = "azkl9er8k7xxdng5rr1tu256ndxsplim";

// const ORIGIN_URL = "http://localhost:3000";
const ORIGIN_URL = "https://rewriting.gitlabpages.inria.fr";

app.use(
  cors({ credentials: true, origin: ORIGIN_URL })
);
app.use(express.json());
app.use(cookieParser());

const uri = "mongodb://rewriting_database:27017/database";
// const uri = "mongodb://localhost:27023/database";

mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));

app.post("/api/register", async (req, res) => {
  const { username, password } = req.body;
  try {
    const userDoc = await User.create({
      username,
      password: bcrypt.hashSync(password, salt),
    });
    res.json(userDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

// Route pour la modification d'un nom d'utilisateur
app.put("/api/user/email/:id", upload.none(), async (req, res) => {
  try {
    const { id } = req.params;
    const { username } = req.body;
    await User.findByIdAndUpdate(id, { username });
    res.json({ message: "Username updated successfully" });
  } catch (e) {
    res.status(400).json(e);
  }
});

function generateRandomPassword(length) {
  return crypto.randomBytes(length).toString("hex");
}

app.post("/api/user/random-password", upload.none(), async (req, res) => {
  try {
    const { username } = req.body;
    const password = generateRandomPassword(10);
    res.json({ message: "User created successfully", password });
    const newUser = await User.create({ username, password });
    //hash password
    newUser.password = bcrypt.hashSync(password, salt);
    await newUser.save();
  } catch (e) {
    res.status(400).json(e);
  }
});

//change password or email
app.put("/api/user/:username", upload.none(), async (req, res) => {
  const { username } = req.params;
  const { name, value } = req.body;
  try {
    const userDoc = await User.findOne({ username });
    if (!userDoc) {
      return res.status(404).json({ message: "User not found" });
    }
    if (name == "email") {
      //set dans la bdd
      userDoc.username = value;
    }
    if (name == "password") {
      userDoc.password = bcrypt.hashSync(value, salt);
    }
    await userDoc.save();
    res.json(userDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

//get users mails
app.get("/api/user", async (req, res) => {
  try {
    const data = await User.find();
    const userData = data.map((user) => ({
      _id: user._id,
      username: user.username,
    }));
    res.json(userData);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.post("/api/login", async (req, res) => {
  const { username, password } = req.body;
  const userDoc = await User.findOne({ username });
  if (!userDoc) {
    res.status(400).json({ message: "User not found" });
  } else {
    const passOk = bcrypt.compareSync(password, userDoc.password);
    if (passOk) {
      jwt.sign({ username, id: userDoc._id }, secret, {}, (err, token) => {
        if (err) throw err;
        res.cookie("token", token).json("ok");
      });
    } else {
      res.status(400).json({ message: "Wrong password" });
    }
  }
});

//token
app.get("/api/profile", (req, res) => {
  const { token } = req.cookies;
  jwt.verify(token, secret, {}, (err, info) => {
    if (err) throw err;
    res.json(info);
  });
});

//get user id
app.get("/api/user/getid/:email", async (req, res) => {
  try {
    const { email } = req.params;
    const user = await User.findOne({ email });
    res.json({ id: user._id });
  } catch (e) {
    res.status(400).json(e);
  }
});

// Route pour la suppression d'un utilisateur
app.delete("/api/user/:id", async (req, res) => {
  try {
    const { id } = req.params;
    await User.findByIdAndDelete(id);
    res.json({ message: "User deleted successfully" });
  } catch (e) {
    res.status(400).json(e);
  }
});

app.post("/api/logout", (req, res) => {
  res.cookie("token", "").json("ok");
});

//researchers data
app.post("/api/researcher", upload.none(), async (req, res) => {
  const { firstname, lastname, description, website } = req.body;
  let { tags } = req.body;

  try {
    tags = JSON.parse(tags);
    const postDoc = await Researcher.create({
      firstname,
      lastname,
      description,
      website,
      tags,
    });
    res.json(postDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.get("/api/researcher", async (req, res) => {
  try {
    const data = await Researcher.find();
    res.json(data);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.put("/api/researcher/:id", upload.none(), async (req, res) => {
  const { id } = req.params;
  const { firstname, lastname, description, website } = req.body;
  let { tags } = req.body;

  try {
    // Parse the incoming tags data as an array
    tags = JSON.parse(tags);
    const postDoc = await Researcher.findByIdAndUpdate(
      id,
      { firstname, lastname, description, website, tags },
      { new: true }
    );
    res.json(postDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.delete("/api/researcher/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const postDoc = await Researcher.findByIdAndDelete(id);
    res.json(postDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

//companies data
app.post("/api/company", upload.none(), async (req, res) => {
  const { name, description, website } = req.body;
  let { tags } = req.body;

  try {
    tags = JSON.parse(tags);
    const companyDoc = await Company.create({
      name,
      description,
      website,
      tags,
    });
    res.json(companyDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.get("/api/company", async (req, res) => {
  try {
    const data = await Company.find();
    res.json(data);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.put("/api/company/:id", upload.none(), async (req, res) => {
  const { id } = req.params;
  const { name, description, website } = req.body;
  let { tags } = req.body;
  try {
    tags = JSON.parse(tags);
    const companyDoc = await Company.findByIdAndUpdate(
      id,
      { name, description, website, tags },
      { new: true }
    );
    res.json(companyDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.delete("/api/company/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const companyDoc = await Company.findByIdAndDelete(id);
    res.json(companyDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

//books
app.post("/api/book", upload.none(), async (req, res) => {
  const { name, description, link } = req.body;
  let { tags } = req.body;

  try {
    tags = JSON.parse(tags);
    const bookDoc = await Book.create({
      name,
      description,
      link,
      tags,
    });
    res.json(bookDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.get("/api/book", async (req, res) => {
  try {
    const data = await Book.find();
    res.json(data);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.put("/api/book/:id", upload.none(), async (req, res) => {
  const { id } = req.params;
  const { name, description, link } = req.body;
  let { tags } = req.body;
  try {
    tags = JSON.parse(tags);
    const bookDoc = await Book.findByIdAndUpdate(
      id,
      { name, description, link, tags },
      { new: true }
    );
    res.json(bookDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.delete("/api/book/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const bookDoc = await Book.findByIdAndDelete(id);
    res.json(bookDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

//course
app.post("/api/course", upload.none(), async (req, res) => {
  const { name, description, link } = req.body;
  let { tags } = req.body;

  try {
    tags = JSON.parse(tags);
    const courseDoc = await Course.create({
      name,
      description,
      link,
      tags,
    });
    res.json(courseDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.get("/api/course", async (req, res) => {
  try {
    const data = await Course.find();
    res.json(data);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.put("/api/course/:id", upload.none(), async (req, res) => {
  const { id } = req.params;
  const { name, description, link } = req.body;
  let { tags } = req.body;
  try {
    tags = JSON.parse(tags);
    const courseDoc = await Course.findByIdAndUpdate(
      id,
      { name, description, link, tags },
      { new: true }
    );
    res.json(courseDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.delete("/api/course/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const courseDoc = await Course.findByIdAndDelete(id);
    res.json(courseDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

//course
app.post("/api/other", upload.none(), async (req, res) => {
  const { name, description, link } = req.body;
  let { tags } = req.body;

  try {
    tags = JSON.parse(tags);
    const otherDoc = await Other.create({
      name,
      description,
      link,
      tags,
    });
    res.json(otherDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.get("/api/other", async (req, res) => {
  try {
    const data = await Other.find();
    res.json(data);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.put("/api/other/:id", upload.none(), async (req, res) => {
  const { id } = req.params;
  const { name, description, link } = req.body;
  let { tags } = req.body;
  try {
    tags = JSON.parse(tags);
    const otherDoc = await Other.findByIdAndUpdate(
      id,
      { name, description, link, tags },
      { new: true }
    );
    res.json(otherDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.delete("/api/other/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const otherDoc = await Other.findByIdAndDelete(id);
    res.json(otherDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

//texts data
app.get("/api/text/:name", async (req, res) => {
  const { name } = req.params;
  try {
    const data = await Text.find({ name });
    res.json(data);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.post("/api/text/:name", upload.none(), async (req, res) => {
  const { name } = req.params;
  const { content } = req.body;
  try {
    const textDoc = await Text.create({
      name,
      content,
    });
    res.json(textDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.put("/api/text/:name/:id", upload.none(), async (req, res) => {
  const { name, id } = req.params;
  const { content } = req.body;
  try {
    const textDoc = await Text.findByIdAndUpdate(
      id,
      { name, content },
      { new: true }
    );
    res.json(textDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

//conference data
app.get("/api/conference", async (req, res) => {
  try {
    const data = await Conference.find();
    res.json(data);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.post("/api/conference", upload.none(), async (req, res) => {
  const { name, description, website } = req.body;
  let { past } = req.body;
  let { upcoming } = req.body;
  let { tags } = req.body;
  try {
    past = past ? JSON.parse(past) : [];
    upcoming = upcoming ? JSON.parse(upcoming) : [];
    tags = JSON.parse(tags);
    const conferenceDoc = await Conference.create({
      name,
      description,
      website,
      past,
      upcoming,
      tags,
    });
    res.json(conferenceDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

//adding past or upcoming conference
app.post("/api/conference/:id", upload.none(), async (req, res) => {
  const { id } = req.params;
  const { type, event } = req.body;
  try {
    if (type !== "past" && type !== "upcoming") {
      throw new Error("Invalid event type");
    }
    const parsedEvent = JSON.parse(event);
    const conferenceDoc = await Conference.findOneAndUpdate(
      { _id: id },
      { $push: { [type]: parsedEvent } },
      { new: true }
    );
    res.json(conferenceDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.delete("/api/conference/:id/event/:eventId", async (req, res) => {
  const { id, eventId } = req.params;
  const { type } = req.query;
  try {
    if (type !== "past" && type !== "upcoming") {
      throw new Error("Invalid event type");
    }
    const conferenceDoc = await Conference.findOneAndUpdate(
      { _id: id },
      { $pull: { [type]: { _id: eventId } } },
      { new: true }
    );
    res.json(conferenceDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.put("/api/conference/edit-type", upload.none(), async (req, res) => {
  try {
    const { id, indexType, type } = req.body;
    let { changes } = req.body;
    changes = JSON.parse(changes);
    const conference = await Conference.findById(id);
    if (!conference) {
      return res.status(404).json({ message: "Conference not found" });
    }
    if (type === "upcoming") {
      conference.upcoming[indexType] = { name: changes[0], url: changes[1] };
    } else if (type === "past") {
      conference.past[indexType] = { name: changes[0], url: changes[1] };
    }
    await conference.save();
    res.status(200).json({ message: "Conference updated successfully" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

app.delete("/api/conference/delete-type", upload.none(), async (req, res) => {
  try {
    const { id, indexType, type } = req.body;
    const conference = await Conference.findById(id);
    if (!conference) {
      return res.status(404).json({ message: "Conference not found" });
    }
    if (type === "upcoming") {
      conference.upcoming.splice(indexType, 1);
    } else if (type === "past") {
      conference.past.splice(indexType, 1);
    }
    await conference.save();
    res.status(200).json({ message: "Conference updated successfully" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

app.put("/api/conference/:id", upload.none(), async (req, res) => {
  const { id } = req.params;
  const { name, description, website } = req.body;
  let { past } = req.body;
  let { upcoming } = req.body;
  let { tags } = req.body;
  try {
    past = JSON.parse(past);
    upcoming = JSON.parse(upcoming);
    tags = JSON.parse(tags);
    const conferenceDoc = await Conference.findByIdAndUpdate(
      id,
      { name, description, website, past, upcoming, tags },
      { new: true }
    );
    res.json(conferenceDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.delete("/api/conference/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const conferenceDoc = await Conference.findByIdAndDelete(id);
    res.json(conferenceDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.get("/api/workshop", async (req, res) => {
  try {
    const data = await Workshop.find();
    res.json(data);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.post("/api/workshop", upload.none(), async (req, res) => {
  const { name, description, website } = req.body;
  let { past } = req.body;
  let { upcoming } = req.body;
  let { tags } = req.body;
  try {
    past = past ? JSON.parse(past) : [];
    upcoming = upcoming ? JSON.parse(upcoming) : [];
    tags = JSON.parse(tags);
    const workshopDoc = await Workshop.create({
      name,
      description,
      website,
      past,
      upcoming,
      tags,
    });
    res.json(workshopDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

//adding past or upcoming workshop
app.post("/api/workshop/:id", upload.none(), async (req, res) => {
  const { id } = req.params;
  const { type, event } = req.body;
  try {
    if (type !== "past" && type !== "upcoming") {
      throw new Error("Invalid event type");
    }
    const parsedEvent = JSON.parse(event);
    const workshopDoc = await Workshop.findOneAndUpdate(
      { _id: id },
      { $push: { [type]: parsedEvent } },
      { new: true }
    );
    res.json(workshopDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.delete("/api/workshop/:id/event/:eventId", async (req, res) => {
  const { id, eventId } = req.params;
  const { type } = req.query;
  try {
    if (type !== "past" && type !== "upcoming") {
      throw new Error("Invalid event type");
    }
    const workshopDoc = await Workshop.findOneAndUpdate(
      { _id: id },
      { $pull: { [type]: { _id: eventId } } },
      { new: true }
    );
    res.json(workshopDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.put("/api/workshop/edit-type", upload.none(), async (req, res) => {
  try {
    const { id, indexType, type } = req.body;
    let { changes } = req.body;
    changes = JSON.parse(changes);
    const workshop = await Workshop.findById(id);
    if (!workshop) {
      return res.status(404).json({ message: "Workshop not found" });
    }
    if (type === "upcoming") {
      workshop.upcoming[indexType] = { name: changes[0], url: changes[1] };
    } else if (type === "past") {
      workshop.past[indexType] = { name: changes[0], url: changes[1] };
    }
    await workshop.save();
    res.status(200).json({ message: "Workshop updated successfully" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

app.delete("/api/workshop/delete-type", upload.none(), async (req, res) => {
  try {
    const { id, indexType, type } = req.body;
    const workshop = await Workshop.findById(id);
    if (!workshop) {
      return res.status(404).json({ message: "Workshop not found" });
    }
    if (type === "upcoming") {
      workshop.upcoming.splice(indexType, 1);
    } else if (type === "past") {
      workshop.past.splice(indexType, 1);
    }
    await workshop.save();
    res.status(200).json({ message: "Workshop updated successfully" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

app.put("/api/workshop/:id", upload.none(), async (req, res) => {
  const { id } = req.params;
  const { name, description, website } = req.body;
  let { past } = req.body;
  let { upcoming } = req.body;
  let { tags } = req.body;
  try {
    past = JSON.parse(past);
    upcoming = JSON.parse(upcoming);
    tags = JSON.parse(tags);
    const workshopDoc = await Workshop.findByIdAndUpdate(
      id,
      { name, description, website, past, upcoming, tags },
      { new: true }
    );
    res.json(workshopDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.delete("/api/workshop/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const workshopDoc = await Workshop.findByIdAndDelete(id);
    res.json(workshopDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

//tools data
app.post("/api/tool", upload.none(), async (req, res) => {
  const { name, description, website, type, fieldOfUse } = req.body;

  try {
    const postDoc = await Tool.create({
      name,
      description,
      website,
      type,
      fieldOfUse,
    });
    res.json(postDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.get("/api/tool", async (req, res) => {
  try {
    const data = await Tool.find();
    res.json(data);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.put("/api/tool/:id", upload.none(), async (req, res) => {
  const { id } = req.params;
  const { name, description, website, type, fieldOfUse } = req.body;

  try {
    // Parse the incoming tags data as an array
    const postDoc = await Tool.findByIdAndUpdate(
      id,
      { name, description, website, type, fieldOfUse },
      { new: true }
    );
    res.json(postDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.delete("/api/tool/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const postDoc = await Tool.findByIdAndDelete(id);
    res.json(postDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

// OpenProblem data
app.post("/api/openproblem", upload.none(), async (req, res) => {
  const { entitled, status, text } = req.body;
  let { tags } = req.body;

  try {
    tags = JSON.parse(tags);
    const postDoc = await OpenProblem.create({
      entitled,
      status,
      text,
      tags,
    });
    res.json(postDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.get("/api/openproblem", async (req, res) => {
  try {
    const data = await OpenProblem.find();
    res.json(data);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.put("/api/openproblem/:id", upload.none(), async (req, res) => {
  const { id } = req.params;
  const { entitled, status, text } = req.body;
  let { tags } = req.body;

  try {
    // Parse the incoming tags data as an array
    tags = JSON.parse(tags);
    const postDoc = await OpenProblem.findByIdAndUpdate(
      id,
      { entitled, status, text, tags },
      { new: true }
    );
    res.json(postDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.delete("/api/openproblem/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const postDoc = await OpenProblem.findByIdAndDelete(id);
    res.json(postDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

// Competition data
app.post("/api/competition", upload.none(), async (req, res) => {
  const { name, website } = req.body;

  try {
    const postDoc = await Competition.create({
      name,
      website,
    });
    res.json(postDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.get("/api/competition", async (req, res) => {
  try {
    const data = await Competition.find();
    res.json(data);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.put("/api/competition/:id", upload.none(), async (req, res) => {
  const { id } = req.params;
  const { name, website } = req.body;

  try {
    const postDoc = await Competition.findByIdAndUpdate(
      id,
      { name, website },
      { new: true }
    );
    res.json(postDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.delete("/api/competition/:id", async (req, res) => {
  const { id } = req.params;
  try {
    const postDoc = await Competition.findByIdAndDelete(id);
    res.json(postDoc);
  } catch (e) {
    res.status(400).json(e);
  }
});

app.get("/", (req, res, next) => {
  res.redirect("http://localhost:3000/rewriting/");
});

app.get("/*", (req, res, next) => {
  res.redirect("http://localhost:3000/rewriting/");
});

const port = 5000;
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
