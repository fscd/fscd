import Box from "@mui/material/Box";
import IntranetNavBar from "../../../components/IntranetNavBar";
import Typography from "@mui/material/Typography";
import { useEffect, useState } from "react";
import DataTable from "../../../components/DataTable";
import AddEntry from "../../../components/AddEntry";
import Button from "@mui/material/Button";
import {
  Breadcrumbs,
  IconButton,
  LinearProgress,
  Tooltip,
} from "@mui/material";
import EditEntry from "../../../components/EditEntry";
import ModeEditIcon from "@mui/icons-material/ModeEdit";
import RefreshIcon from "@mui/icons-material/Refresh";
import DeleteIcon from "@mui/icons-material/Delete";
import DeleteEntry from "../../../components/DeleteEntry";
import DrawerHeader from "../../../components/DrawerHeader";
import { NavLink } from "react-router-dom";

const EditToolsPage = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [wantToEdit, setWantToEdit] = useState(false);
  const [wantToDelete, setWantToDelete] = useState(false);
  const [counter, setCounter] = useState(0);
  const [dataEdited, setDataEdited] = useState([]);
  const name = "tool";

  const fetchData = () => {
    setLoading(true);
    fetch(API_URL + name).then((response) => {
      response.json().then((data) => {
        setData(data);
      });
      response.ok && setLoading(false);
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    fetchData();
    setWantToEdit(false);
    setWantToDelete(false);
  }, []);

  //type, fieldOfUse

  const filteredData = data.map((row, index) => ({
    id: index + 1,
    _id: row._id,
    name: row.name,
    description: row.description,
    website: row.website,
    type: row.type,
    fieldOfUse: row.fieldOfUse,
  }));

  const entries = [
    {
      name: "name",
      displayname: "Name",
      type: "short-text",
    },
    {
      name: "description",
      displayname: "Description",
      type: "multiline",
    },
    {
      name: "website",
      displayname: "Website",
      type: "short-text",
    },
    {
      name: "type",
      displayname: "Type",
      type: "select",
      fields: ["Industrial", "Free"],
    },
    {
      name: "fieldOfUse",
      displayname: "Field of use",
      type: "select",
      fields: ["Academic", "Non-academic"],
    },
  ];

  const columns = [
    {
      field: "id",
      headerName: "ID",
      width: 70,
      renderCell: (params) => (
        <div style={{ whiteSpace: "normal" }}>{params.value}</div>
      ),
    },
    {
      field: "name",
      headerName: "Name",
      width: 130,
      renderCell: (params) => (
        <div style={{ whiteSpace: "normal" }}>{params.value}</div>
      ),
    },
    {
      field: "description",
      headerName: "Description",
      width: 200,
      renderCell: (params) => (
        <div style={{ whiteSpace: "normal" }}>{params.value}</div>
      ),
    },
    {
      field: "website",
      headerName: "Website",
      width: 160,
      renderCell: (params) => (
        <div style={{ whiteSpace: "normal" }}>{params.value}</div>
      ),
    },
    {
      field: "type",
      headerName: "Type",
      width: 160,
      renderCell: (params) => (
        <div style={{ whiteSpace: "normal" }}>{params.value}</div>
      ),
    },
    {
      field: "fieldOfUse",
      headerName: "Field of use",
      width: 160,
      renderCell: (params) => (
        <div style={{ whiteSpace: "normal" }}>{params.value}</div>
      ),
    },
    {
      field: "edit",
      headerName: "",

      renderCell: (params) => {
        return (
          <Tooltip title="Edit">
            <IconButton
              style={{ color: "#1565C0" }}
              onClick={(evt) => {
                evt.preventDefault();
                setWantToEdit(true);
                setWantToDelete(false);
                setCounter((prevCounter) => prevCounter + 1);
                setDataEdited(filteredData[params.row.id - 1]);
              }}
            >
              <ModeEditIcon />
            </IconButton>
          </Tooltip>
        );
      },
    },
    {
      field: "delete",
      headerName: "",

      renderCell: (params) => {
        return (
          <Tooltip title="Delete">
            <IconButton
              style={{ background: "#ffcdcd" }}
              onClick={(evt) => {
                evt.preventDefault();
                setWantToDelete(true);
                setWantToEdit(false);
                setCounter((prevCounter) => prevCounter + 1);
                setDataEdited(filteredData[params.row.id - 1]);
              }}
            >
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        );
      },
    },
  ];

  return (
    <Box sx={{ display: "flex", width: "100%" }}>
      <IntranetNavBar entitled="Edit website > Tools" />
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          p: 3,
          display: "flex",
          flexDirection: "column",
          gap: "10px",
          maxWidth: "100%",
          overflowX: "scroll",
        }}
      >
        <DrawerHeader />
        <Breadcrumbs aria-label="breadcrumb">
          <NavLink
            underline="none"
            color="black"
            to="/rewriting/intranet/edit-website"
          >
            Edit website
          </NavLink>
          <Typography color="text.primary">Tools</Typography>
        </Breadcrumbs>
        {wantToEdit && (
          <>
            <EditEntry
              name={name}
              entries={entries}
              key={"editentry" + counter}
              data={dataEdited}
            />
          </>
        )}
        {wantToDelete && (
          <>
            <DeleteEntry
              name={name}
              entries={entries}
              key={"deleteentry" + counter}
              data={dataEdited}
            />
          </>
        )}

        {loading ? (
          <>
            Loading data...
            <LinearProgress />{" "}
          </>
        ) : (
          <>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end",
                gap: "10px",
              }}
            >
              <Button
                onClick={fetchData}
                variant="contained"
                style={{
                  boxShadow: "none",
                  textTransform: "none",
                  fontSize: 16,
                  padding: "6px 12px",
                  border: "1px solid",
                  marginBottom: "10px",
                  lineHeight: 1.5,
                }}
              >
                Refresh <RefreshIcon />
              </Button>

              <AddEntry name={name} entries={entries} />
            </Box>
            <DataTable name={name} columns={columns} rows={filteredData} />
          </>
        )}
      </Box>
    </Box>
  );
};

export default EditToolsPage;
