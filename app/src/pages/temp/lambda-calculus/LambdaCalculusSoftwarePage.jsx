// import React, { useEffect } from "react";
// import NavBar from "../../components/NavBar";
// import Title from "../../components/Title";
// import Section from "../../components/Section";
// import Path from "../../components/Path";
// import Footer from "../../components/Footer";

// const LambdaCalculusSoftwarePage = () => {
//   useEffect(() => {
//     window.scrollTo(0, 0);
//   }, []);
//   const sections = [
//     {
//       title: "Section",
//       content: ["Text"],
//     },
//     {
//       title: "Section",
//       content: ["Text"],
//     },
//     {
//       title: "Section",
//       content: ["Text"],
//     },
//     {
//       title: "Section",
//       content: ["Text"],
//     },
//   ];

//   const paths = [
//     {
//       name: "Lambda Calculus",
//       path: "/rewriting/temp/lambda-calculus/introduction",
//     },
//     {
//       name: "Software",
//       path: "/rewriting/temp/lambda-calculus/software",
//     },
//   ];

//   return (
//     <div className="container">
//       <NavBar
//         theme="lambda-calculus"
//         paths={[
//           {
//             id: 1,
//             name: "Introduction",
//             path: "/rewriting/temp/lambda-calculus/introduction",
//           },
//           {
//             id: 2,
//             name: "Software",
//             path: "/rewriting/temp/lambda-calculus/software",
//           },
//           {
//             id: 3,
//             name: "Ressources",
//             path: "/rewriting/temp/lambda-calculus/ressources",
//           },
//         ]}
//       />
//       <div className="main">
//         <Path paths={paths} branch="lambda-calculus" />
//         <Title title="Software" />
//         {sections.map((section, index) => (
//           <Section
//             key={index}
//             title={section.title}
//             content={section.content}
//           />
//         ))}
//       </div>
//       <Footer />
//     </div>
//   );
// };

// export default LambdaCalculusSoftwarePage;
