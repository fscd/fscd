const mongoose = require("mongoose");
const { Schema, model } = mongoose;

const CompetitionSchema = new Schema(
  {
    name: String,
    website: String,
  },
  {
    timestamps: true,
  }
);

const AddCompetitionSchemaModel = model("Competition", CompetitionSchema);

module.exports = AddCompetitionSchemaModel;
