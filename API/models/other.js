const mongoose = require("mongoose");
const { Schema, model } = mongoose;

const OtherSchema = new Schema(
  {
    name: String,
    description: String,
    link: String,
    tags: [String],
  },
  {
    timestamps: true,
  }
);

const AddOtherSchemaModel = model("Other", OtherSchema);

module.exports = AddOtherSchemaModel;
