const mongoose = require("mongoose");
const { Schema, model } = mongoose;

const CompanySchema = new Schema(
  {
    name: String,
    description: String,
    website: String,
    tags: [String],
  },
  {
    timestamps: true,
  }
);

const AddCompanySchemaModel = model("Company", CompanySchema);

module.exports = AddCompanySchemaModel;
