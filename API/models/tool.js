const mongoose = require("mongoose");
const { Schema, model } = mongoose;

const ToolSchema = new Schema(
  {
    name: String,
    description: String,
    website: String,
    type: String,
    fieldOfUse: String,
  },
  {
    timestamps: true,
  }
);

const AddToolSchemaModel = model("Tool", ToolSchema);

module.exports = AddToolSchemaModel;
