export const TempLambdaCalculusPath = [
  {
    id: 1,
    name: "Introduction",
    path: "/rewriting/temp/lambda-calculus/introduction",
  },
  {
    id: 2,
    name: "Ressources",
    path: "/rewriting/temp/lambda-calculus/ressources",
  },
];
