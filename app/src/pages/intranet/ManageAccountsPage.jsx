import Box from "@mui/material/Box";
import IntranetNavBar from "../../components/IntranetNavBar";
import { styled } from "@mui/material/styles";
import { useEffect, useState } from "react";
import { Button, IconButton, LinearProgress, Tooltip } from "@mui/material";
import DataTable from "../../components/DataTable";
import ModeEditIcon from "@mui/icons-material/ModeEdit";
import DeleteIcon from "@mui/icons-material/Delete";
import RefreshIcon from "@mui/icons-material/Refresh";
import EditEntry from "../../components/EditEntry";
import DeleteEntry from "../../components/DeleteEntry";
import AddEntry from "../../components/AddEntry";

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

const ManageAccountsPage = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [wantToEdit, setWantToEdit] = useState(false);
  const [wantToDelete, setWantToDelete] = useState(false);
  const [dataEdited, setDataEdited] = useState([]);
  const name = "user/random-password";

  const [counter, setCounter] = useState(0);

  const fetchData = () => {
    setLoading(true);
    fetch("https://rewriting.inria.fr/api/user").then((response) => {
      response.json().then((data) => {
        setData(data);
      });
      response.ok && setLoading(false);
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    fetchData();
  }, []);

  const filteredData = data.map((row, index) => ({
    id: index + 1,
    _id: row._id,
    username: row.username, // Utiliser la syntaxe d'accès aux propriétés d'un objet
  }));

  const entries = [
    {
      name: "username",
      displayname: "Email",
      type: "email",
    },
  ];

  const columns = [
    {
      field: "id",
      headerName: "ID",
      width: 30,
      renderCell: (params) => (
        <div style={{ whiteSpace: "normal" }}>{params.value}</div>
      ),
    },
    {
      field: "username",
      headerName: "Email",
      width: 300,
      renderCell: (params) => (
        <div style={{ whiteSpace: "normal" }}>{params.value}</div>
      ),
    },
    {
      field: "edit",
      headerName: "",

      renderCell: (params) => {
        return (
          <Tooltip title="Edit">
            <IconButton
              style={{ color: "#1565C0" }}
              onClick={(evt) => {
                evt.preventDefault();
                setWantToEdit(true);
                setWantToDelete(false);
                setCounter((prevCounter) => prevCounter + 1);
                setDataEdited(filteredData[params.row.id - 1]);
              }}
            >
              <ModeEditIcon />
            </IconButton>
          </Tooltip>
        );
      },
    },
    {
      field: "delete",
      headerName: "",

      renderCell: (params) => {
        return (
          <Tooltip title="Delete">
            <IconButton
              style={{ background: "#ffcdcd" }}
              onClick={(evt) => {
                evt.preventDefault();
                setWantToDelete(true);
                setWantToEdit(false);
                setCounter((prevCounter) => prevCounter + 1);
                setDataEdited(filteredData[params.row.id - 1]);
              }}
            >
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        );
      },
    },
  ];

  return (
    <Box sx={{ display: "flex", width: "100%" }}>
      <IntranetNavBar entitled="Manage accounts" />
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          p: 3,
          display: "flex",
          flexDirection: "column",
          gap: "10px",
          maxWidth: "100%",
          overflowX: "scroll",
        }}
      >
        <DrawerHeader />
        {wantToEdit && (
          <>
            <EditEntry
              name={"user/email"}
              entries={entries}
              key={"editentry" + counter}
              data={dataEdited}
            />
          </>
        )}
        {wantToDelete && (
          <>
            <DeleteEntry
              name={"user"}
              entries={entries}
              key={"editentry" + counter}
              data={dataEdited}
            />
          </>
        )}
        {loading ? (
          <>
            Loading data...
            <LinearProgress />{" "}
          </>
        ) : (
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "flex-end",
              gap: "10px",
            }}
          >
            <Button
              onClick={fetchData}
              variant="contained"
              style={{
                boxShadow: "none",
                textTransform: "none",
                fontSize: 16,
                padding: "6px 12px",
                border: "1px solid",
                marginBottom: "10px",
                lineHeight: 1.5,
              }}
            >
              Refresh <RefreshIcon />
            </Button>
            <AddEntry name={name} entries={entries} />
          </Box>
        )}
        <DataTable name={name} columns={columns} rows={filteredData} />
      </Box>
    </Box>
  );
};

export default ManageAccountsPage;
