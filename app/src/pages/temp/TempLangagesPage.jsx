import React, { useEffect } from "react";
import Title from "../../components/Title";
import Card from "../../components/Card";
import Path from "../../components/Path";
import Footer from "../../components/Footer";

const TempLanguagesPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const links = [
    {
      title: "Language n°1",
      url: "",
      tags: ["Programming", "Rewriting"],
    },
    {
      title: "Language n°2",
      url: "",
      tags: ["Programming", "Lambda Calculus"],
    },
    {
      title: "Language n°3",
      url: "",
      tags: ["Programming", "Logic"],
    },
    {
      title: "Language n°4",
      url: "",
      tags: ["Proof", "Logic"],
    },
    {
      title: "Language n°5",
      url: "",
      tags: ["Proof", "Lambda Calculus"],
    },
    {
      title: "Language n°6",
      url: "",
      tags: ["Proof", "Rewriting"],
    },
  ];

  const paths = [
    {
      name: "Home",
      path: "/rewriting/temp",
    },
    {
      name: "Languages",
      path: "/rewriting/temp/languages",
    },
  ];

  return (
    <div className="container">
      <div className="main no-nav-bar">
        <Path paths={paths} />
        <Title title="Programming and proof languages" />
        <div className="industrial-applications-cards-container">
          {links.map(({ title, url, tags }) => (
            <Card title={title} url={url} tags={tags} />
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default TempLanguagesPage;
