import React from "react";
import { NavLink } from "react-router-dom";

const Footer = () => {
  return (
    <div className="footer">
      <div className="copyright">
        <p>Hosted by Gitlab Inria, 2023</p>
      </div>
      <div className="authors">
        <p>
          Made by{" "}
          <NavLink
            to="http://www-sop.inria.fr/members/Luigi.Liquori"
            target="_blank"
          >
            Luigi Liquori
          </NavLink>
          , Inria {" & "}
          <NavLink to="https://www.alexis-rosset.fr" target="_blank">
            Alexis Rosset
          </NavLink>
          , Université Nice Côte d'Azur
        </p>
        <p>
          Credits to{" "}
          <NavLink to="https://gitlab.inria.fr/ptempier" target="_blank">
            Pascal Tempier
          </NavLink>
          {", "}
          <NavLink
            to="https://www.cs.ru.nl/~cynthiakop/index_en.html"
            target="_blank"
          >
            Cynthia Kop
          </NavLink>
          {", "}
          <NavLink to="https://gitlab.inria.fr/egagnon" target="_blank">
            Nachum Dershowitz
          </NavLink>{" "}
          and{" "}
          <NavLink
            to="https://perso.ens-lyon.fr/pierre.lescanne/"
            target="_blank"
          >
            Pierre Lescanne
          </NavLink>
        </p>
      </div>
    </div>
  );
};

export default Footer;
