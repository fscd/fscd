# About this site

**Complete "Rewriting" of the website** around the scientific community on **"Term Rewriting"** http://rewriting.loria.fr/ and its extension to the scientific community on "Typed Lambda Calculus" and possibily on the scientific community on "Logic and Computation".
The two/three communities clearly have a non empty intersection of researchers, and, as such, in 2016 they decided to merge the two well-established international conferences in theoretical computer science on Rewriting Techniques and Applications (RTA) and on Typed Lambda-calculus and Applications (TLCA) into a single conference, called **FSCD** (Formal Structures for Computation and Deduction), https://fscd-conference.org/. FSCD is a new conference, born in 2016 from the merger of RTA and TLCA:

- RTA (Rewriting Techniques and Applications, https://dblp.org/db/conf/rta/index.html) ran biannually from 1985 to 1993, and annually from 1995 to 2015.
- TLCA (Typed Lambda-Calculi and Applications https://dblp.org/db/conf/tlca/index.html) ran biannually from 1993 to 2015.

The new site will be hosted through the gitlab Inria. Full documentation about the web tree structure (extranet and intranet) and the full features will be also provided in a separate PDF. The web site will be "dynamic" featuring also a data base and an intranet that can be accessed via suitable registration and login. A web mailing list for rewriting is actually hosted by sympa server list in Ens-Lyon, and another for lambda-calculus will be hosted somewhere (it could be simpler to host it using the same sympa server list). TBD if both servers could be hosted @Inria.

Actually the web and all its features is managed and fully bootstrapped by Liquori and Rosset, but we strongly plan to extend the number of Owners and Maintainer, followinf the gitlab hierachy to ensure the liveness and the mainenance of the web site (in terms of years).

Adding also the third community around Logic and Computation (Curry-howard isomorphism) is worth to be discussed: this would make this site a real "Agora" for all Theoretical Computer Science issues related with Abstract Reduction Systems, Term Rewriting, Lambda-calculus, Types, Logic and Computation, and, in general, foundation of Programming and Proof languages, ...

The site is "responsive" on smartphones and other web devices.

Last remark: we asked the Inria system management to revers a domain name, called **rewriting.inria.fr** as the main home.

## Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app)

## Instructions to run the project in local

Download NodeJS (https://nodejs.org/) and execute it.

Then, execute the following commands :

### `cd app`

Navigate to the folder named "app".

### `npm i`

Install the dependencies.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

#### Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

## Authors

Luigi LIQUORI and Alexis ROSSET.
