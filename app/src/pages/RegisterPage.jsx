import React, { useState } from "react";
import Title from "../components/Title";
import Footer from "../components/Footer";
// import { useNavigate } from "react-router-dom";
import TextField from "@mui/material/TextField";
import { Button } from "@mui/material";
// import bcrypt from "bcryptjs";
// import Axios from "axios";
import API_URL from "../constants";

const RegisterPage = () => {
  // const [data, setData] = useState([]);
  // const [email, setEmail] = useState("");
  // const [password, setPassword] = useState("");
  // const [error, setError] = useState("");
  // const [haveApi, setApi] = useState(false);
  // const [loggedIn, setLoggedIn] = useState(false);

  // const navigate = useNavigate();
  // const saltRounds = 10;

  // useEffect(() => {
  //   const fetchData = async () => {
  //     try {
  //       const response = await Axios.get(API_URL + "login");
  //       setData(response.data);
  //       setApi(true);
  //     } catch (error) {
  //       console.log("error : " + error);
  //     }
  //   };

  //   if (!haveApi) {
  //     fetchData();
  //   } else if (localStorage.getItem("authenticated")) {
  //     console.log("redirect to intranet");
  //     navigate("/rewriting/intranet");
  //   }
  // });

  // async function SignUpForm(e) {
  //   e.preventDefault();
  //   const hashedPassword = bcrypt.hashSync(password, saltRounds);
  //   window.localStorage.setItem(
  //     "login",
  //     JSON.stringify({ email, hashedPassword })
  //   );
  //   window.localStorage.setItem("post-request", true);
  //   if (localStorage.getItem("post-request")) {
  //     localStorage.removeItem("post-request");
  //     const formData = {
  //       email: email,
  //       password: hashedPassword,
  //     };
  //     await Axios.post("https://rewriting.inria.fr/api/login", formData)
  //       .then(function (response) {
  //         console.log(response);
  //       })
  //       .catch(function (error) {
  //         console.log(error);
  //       });
  //   }
  // }

  // async function LoginForm(e) {
  //   e.preventDefault();
  //   console.log("Api pulled: " + haveApi);
  //   console.log("json: ");
  //   data.forEach((user) => {
  //     console.log(user.email + " " + user.password);
  //   });
  //   data.forEach((user) => {
  //     console.log("user: " + user.email);
  //     console.log(user.password + " " + password);
  //     const result = bcrypt.compareSync(password, user.password);
  //     if (result) {
  //       console.log("User has an account");
  //       // setLoggedIn(true);
  //       localStorage.setItem("authenticated", true);
  //       console.log("in login : redirect to intranet");
  //       navigate("/rewriting/intranet");
  //     } else {
  //       console.log("user doesn't have an account");
  //     }
  //   });
  // }

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  async function register(ev) {
    ev.preventDefault();
    const response = await fetch("https://rewriting.inria.fr/api/register", {
      method: "POST",
      body: JSON.stringify({ username, password }),
      headers: { "Content-Type": "application/json" },
    });
    if (response.status === 200) {
      alert("Register successful");
    } else {
      alert("Register failed");
    }
  }

  return (
    <div className="login-page">
      <div className="login-card">
        <Title title="Intranet" />

        {/* {error ? (
          <TextField
            onChange={(e) => setEmail(e.target.value)}
            margin="normal"
            fullWidth
            error
            helperText="Incorrect entry."
            name="Email"
            label="Email"
            type="email"
            id="email"
            autoComplete="current-email"
            maxRows={1}
          />
        ) : ( */}
        <form className="register" onSubmit={register}>
          <TextField
            // onChange={(e) => setEmail(e.target.value)}
            margin="normal"
            required
            fullWidth
            name="username"
            label="Email"
            type="email"
            id="email"
            autoComplete="current-email"
            maxRows={1}
            onChange={(e) => setUsername(e.target.value)}
          />
          {/* )} */}
          {/* {error ? (
          <TextField
            onChange={(e) => setPassword(e.target.value)}
            margin="normal"
            error
            helperText="Incorrect entry."
            fullWidth
            name="Password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            maxRows={1}
          />
        ) : ( */}
          <TextField
            // onChange={(e) => setPassword(e.target.value)}
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            maxRows={1}
            onChange={(e) => setPassword(e.target.value)}
          />
          {/* )} */}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
            // onClick={(e) => SignUpForm(e)}
          >
            Register
          </Button>
        </form>
      </div>
      <Footer />
    </div>
  );
};

export default RegisterPage;
