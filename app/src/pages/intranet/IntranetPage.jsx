import React, { useEffect } from "react";
import Box from "@mui/material/Box";
import IntranetNavBar from "../../components/IntranetNavBar";
import Typography from "@mui/material/Typography";
import { styled } from "@mui/material/styles";

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

const IntranetPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  });

  return (
    <Box sx={{ display: "flex" }}>
      <IntranetNavBar entitled="Intranet Home" />
      <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
        <DrawerHeader />
        <Typography paragraph>You are logged into the intranet.</Typography>
      </Box>
    </Box>
  );
};

export default IntranetPage;
