import React, { useEffect } from "react";
import Title from "../../../components/Title";
import Card from "../../../components/Card";
import Path from "../../../components/Path";
import Footer from "../../../components/Footer";
import NavBar from "../../../components/temp/NavBar";

const FreeToolsPage = (props) => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const links = [
    {
      title: "Free tool n°1",
      url: "",
      tags: ["Rewriting", "Logic"],
    },
    {
      title: "Free tool n°2",
      url: "",
      tags: ["Rewriting", "Lambda Calculus"],
    },
    {
      title: "Free tool n°3",
      url: "",
      tags: ["Lambda Calculus"],
    },
    {
      title: "Free tool n°4",
      url: "",
      tags: ["Logic"],
    },
    {
      title: "Free tool n°5",
      url: "",
      tags: ["Rewriting"],
    },
    {
      title: "Free tool n°6",
      url: "",
      tags: ["Rewriting"],
    },
    {
      title: "Free tool n°7",
      url: "",
      tags: ["Rewriting", "Logic", "Lambda Calculus"],
    },
    {
      title: "Free tool n°8",
      url: "",
      tags: ["Logic"],
    },
  ];

  const paths = [
    {
      name: "Tools",
      path: "/rewriting/tools",
    },
    {
      name: "Free tools",
      path: "/rewriting/tools/free-tools",
    },
  ];

  return (
    <div className="container">
      <NavBar theme="rewriting" />
      <div className="main">
        <Path paths={paths} branch="rewriting" />
        <Title title="Free tools" />
        <div className="industrial-applications-cards-container">
          {links.map(({ title, url, tags }) => (
            <Card title={title} url={url} tags={tags} />
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default FreeToolsPage;
