import React, { useEffect } from "react";
import Title from "../../components/Title";
import Card from "../../components/Card";
import Path from "../../components/Path";
import Footer from "../../components/Footer";

const TempOpenProblemsPage = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  const links = [
    {
      title: "Open problem n°1",
      url: "",
      tags: ["Rewriting", "Logic"],
    },
    {
      title: "Open problem n°2",
      url: "",
      tags: ["Rewriting", "Lambda Calculus"],
    },
    {
      title: "Open problem n°3",
      url: "",
      tags: ["Lambda Calculus"],
    },
    {
      title: "Open problem n°4",
      url: "",
      tags: ["Logic"],
    },
    {
      title: "Open problem n°5",
      url: "",
      tags: ["Rewriting"],
    },
    {
      title: "Open problem n°6",
      url: "",
      tags: ["Rewriting"],
    },
    {
      title: "Open problem n°7",
      url: "",
      tags: ["Rewriting", "Logic", "Lambda Calculus"],
    },
    {
      title: "Open problem n°8",
      url: "",
      tags: ["Logic"],
    },
  ];

  const paths = [
    {
      name: "Home",
      path: "/rewriting/temp",
    },
    {
      name: "Open problems",
      path: "/rewriting/temp/open-problems",
    },
  ];

  return (
    <div className="container">
      <div className="main no-nav-bar">
        <Path paths={paths} />
        <Title title="Open problems" />
        <div className="industrial-applications-cards-container">
          {links.map(({ title, url, tags }) => (
            <Card title={title} url={url} tags={tags} />
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default TempOpenProblemsPage;
