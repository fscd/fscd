const mongoose = require("mongoose");
const { Schema, model } = mongoose;

const BookSchema = new Schema(
  {
    name: String,
    description: String,
    link: String,
    tags: [String],
  },
  {
    timestamps: true,
  }
);

const AddBookSchemaModel = model("Book", BookSchema);

module.exports = AddBookSchemaModel;
