import Box from "@mui/material/Box";
import IntranetNavBar from "../../../components/IntranetNavBar";
import Typography from "@mui/material/Typography";
import { useEffect, useState } from "react";
import DataTable from "../../../components/DataTable";
import AddEntry from "../../../components/AddEntry";
import Button from "@mui/material/Button";
import {
  Breadcrumbs,
  IconButton,
  LinearProgress,
  Tooltip,
} from "@mui/material";
import EditEntry from "../../../components/EditEntry";
import ModeEditIcon from "@mui/icons-material/ModeEdit";
import RefreshIcon from "@mui/icons-material/Refresh";
import DeleteIcon from "@mui/icons-material/Delete";
import DeleteEntry from "../../../components/DeleteEntry";
import DrawerHeader from "../../../components/DrawerHeader";
import { NavLink } from "react-router-dom";

const EditResearchersPage = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [wantToEdit, setWantToEdit] = useState(false);
  const [wantToDelete, setWantToDelete] = useState(false);
  const [counter, setCounter] = useState(0);
  const [dataEdited, setDataEdited] = useState([]);
  const name = "researcher";

  const fetchData = () => {
    setLoading(true);
    fetch(API_URL + name).then((response) => {
      response.json().then((data) => {
        setData(data);
      });
      response.ok && setLoading(false);
    });
  };

  useEffect(() => {
    window.scrollTo(0, 0);
    fetchData();
    setWantToEdit(false);
    setWantToDelete(false);
  }, []);

  const filteredData = data.map((row, index) => ({
    id: index + 1,
    _id: row._id,
    firstname: row.firstname,
    lastname: row.lastname,
    description: row.description,
    website: row.website,
    tags: row.tags,
  }));

  const entries = [
    {
      name: "firstname",
      displayname: "First name",
      type: "short-text",
    },
    {
      name: "lastname",
      displayname: "Last name",
      type: "short-text",
    },
    {
      name: "description",
      displayname: "Description",
      type: "multiline",
    },
    {
      name: "website",
      displayname: "Website",
      type: "short-text",
    },
    {
      name: "tags",
      displayname: "Tag",
      type: "short-text",
    },
  ];

  const columns = [
    {
      field: "id",
      headerName: "ID",
      width: 70,
      renderCell: (params) => (
        <div style={{ whiteSpace: "normal" }}>{params.value}</div>
      ),
    },
    {
      field: "firstname",
      headerName: "First name",
      width: 130,
      renderCell: (params) => (
        <div style={{ whiteSpace: "normal" }}>{params.value}</div>
      ),
    },
    {
      field: "lastname",
      headerName: "Last name",
      width: 130,
      renderCell: (params) => (
        <div style={{ whiteSpace: "normal" }}>{params.value}</div>
      ),
    },
    {
      field: "description",
      headerName: "Description",
      width: 200,
      renderCell: (params) => (
        <div style={{ whiteSpace: "normal" }}>{params.value}</div>
      ),
    },
    {
      field: "website",
      headerName: "Website",
      width: 160,
      renderCell: (params) => (
        <div style={{ whiteSpace: "normal" }}>{params.value}</div>
      ),
    },
    {
      field: "tags",
      headerName: "Tags",
      width: 160,
      renderCell: (params) => (
        <div style={{ whiteSpace: "normal" }}>{params.value.join(", ")}</div>
      ),
    },
    {
      field: "edit",
      headerName: "",

      renderCell: (params) => {
        return (
          <Tooltip title="Edit">
            <IconButton
              style={{ color: "#1565C0" }}
              onClick={(evt) => {
                evt.preventDefault();
                setWantToEdit(true);
                setWantToDelete(false);
                setCounter((prevCounter) => prevCounter + 1);
                setDataEdited(filteredData[params.row.id - 1]);
              }}
            >
              <ModeEditIcon />
            </IconButton>
          </Tooltip>
        );
      },
    },
    {
      field: "delete",
      headerName: "",

      renderCell: (params) => {
        return (
          <Tooltip title="Delete">
            <IconButton
              style={{ background: "#ffcdcd" }}
              onClick={(evt) => {
                evt.preventDefault();
                setWantToDelete(true);
                setWantToEdit(false);
                setCounter((prevCounter) => prevCounter + 1);
                setDataEdited(filteredData[params.row.id - 1]);
              }}
            >
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        );
      },
    },
  ];

  return (
    <Box sx={{ display: "flex", width: "100%" }}>
      <IntranetNavBar entitled="Edit website > Researchers" />
      <Box
        component="main"
        sx={{
          flexGrow: 1,
          p: 3,
          display: "flex",
          flexDirection: "column",
          gap: "10px",
          maxWidth: "100%",
          overflowX: "scroll",
        }}
      >
        <DrawerHeader />
        <Breadcrumbs aria-label="breadcrumb">
          <NavLink
            underline="none"
            color="black"
            to="/rewriting/intranet/edit-website"
          >
            Edit website
          </NavLink>
          <Typography color="text.primary">Researchers</Typography>
        </Breadcrumbs>
        {wantToEdit && (
          <>
            <EditEntry
              name={name}
              entries={entries}
              key={"editentry" + counter}
              data={dataEdited}
            />
          </>
        )}
        {wantToDelete && (
          <>
            <DeleteEntry
              name={name}
              entries={entries}
              key={"deleteentry" + counter}
              data={dataEdited}
            />
          </>
        )}

        {loading ? (
          <>
            Loading data...
            <LinearProgress />{" "}
          </>
        ) : (
          <>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end",
                gap: "10px",
              }}
            >
              <Button
                onClick={fetchData}
                variant="contained"
                style={{
                  boxShadow: "none",
                  textTransform: "none",
                  fontSize: 16,
                  padding: "6px 12px",
                  border: "1px solid",
                  marginBottom: "10px",
                  lineHeight: 1.5,
                }}
              >
                Refresh <RefreshIcon />
              </Button>

              <AddEntry name={name} entries={entries} />
            </Box>
            <DataTable name={name} columns={columns} rows={filteredData} />
          </>
        )}
      </Box>
    </Box>
  );
};

export default EditResearchersPage;
