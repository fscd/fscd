const mongoose = require("mongoose");
const { Schema, model } = mongoose;

const ResearcherSchema = new Schema(
  {
    firstname: String,
    lastname: String,
    description: String,
    website: String,
    tags: [String],
  },
  {
    timestamps: true,
  }
);

const AddResearcherSchemaModel = model("Researcher", ResearcherSchema);

module.exports = AddResearcherSchemaModel;
