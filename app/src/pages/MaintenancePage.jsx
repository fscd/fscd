import React from 'react';
import { Box, Typography, Container, Icon, CssBaseline } from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import inriaLogo from '../assets/img/inria.png';
import universityLogo from '../assets/img/logo_cote_dazur.png';

const theme = createTheme({
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        body: {
          margin: 0,
          padding: 0,
          backgroundColor: '#f0f0f0', // Your desired background color
          width: '100%',
          height: '100%',
        },
      },
    },
  },
});

const MaintenancePage = () => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Container sx={styles.container}>
        <Box component="img" src={inriaLogo} alt="Inria Logo" sx={styles.leftLogo} />
        <Box component="img" src={universityLogo} alt="University Logo" sx={styles.rightLogo} />
        <Box sx={styles.content}>
          <Icon sx={styles.icon}>🚧</Icon>
          <Typography variant="h1" sx={styles.message}>Work in prooooooooooogress</Typography>
          <Typography variant="body1" sx={styles.text}>
            The new rewriting website version 1.0 (beta), and its contents will be launched in September 2024.
            Please refer to Luigi.Liquori@inria.fr for any information.
          </Typography>
        </Box>
      </Container>
    </ThemeProvider>
  );
};

const styles = {
  container: {
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: '100vh',
    width: '100vw',
    textAlign: 'center',
    padding: '20px',
  },
  leftLogo: {
    position: 'absolute',
    top: '20px',
    left: '20px',
    width: { xs: '100px', md: '150px' },
    height: 'auto',
  },
  rightLogo: {
    position: 'absolute',
    top: '20px',
    right: '20px',
    width: { xs: '100px', md: '150px' },
    height: 'auto',
  },
  content: {
    flex: '1',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    padding: '20px',
    backgroundColor: 'transparent', // Ensure the content background is transparent
  },
  icon: {
    fontSize: { xs: '10vw', md: '8vw' },
    color: '#ffcc00',
    marginBottom: '10px',
  },
  message: {
    fontSize: { xs: '6vw', md: '4vw' },
    color: '#666',
    margin: '10px 0',
  },
  text: {
    fontSize: { xs: '4vw', md: '1vw' },
    color: '#666',
    margin: '10px 0',
  },
};

export default MaintenancePage;
